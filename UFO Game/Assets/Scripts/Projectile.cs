﻿using UnityEngine;
using System.Collections;

public enum projectileType
{
	playerProjectile,
	phase2Projectile
}

public class Projectile : MonoBehaviour
{

	

	public projectileType whatAmI;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player") && whatAmI == projectileType.phase2Projectile) {
			other.GetComponentInParent<Pawn> ().Attacked ();
		} else if (other.gameObject.CompareTag ("Enemy") && whatAmI == projectileType.playerProjectile) {
			other.GetComponentInParent<Pawn> ().Attacked ();
		}
	}
}
