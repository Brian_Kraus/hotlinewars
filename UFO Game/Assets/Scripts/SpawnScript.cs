﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour
{

	public GameObject basicEnemy;
	public GameObject phase2Enemy;
	public GameObject Player;
	public GameObject spawnBoxY;
	public GameObject spawnBoxX;
	public GameObject Origin;
	public int safeDistance;


	private BasicEnemyScript script;

	// Use this for initialization
	void Start ()
	{
		
		SpawnBasicEnemy ();
		SpawnBasicEnemy ();
		SpawnPhaseTwoEnemy ();
		SpawnPhaseTwoEnemy ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void SpawnPhaseTwoEnemy ()
	{
		Vector2 spawnLocation = new Vector2 (
			                        Random.Range (Origin.transform.position.x, spawnBoxX.transform.position.x), 
			                        Random.Range (Origin.transform.position.y, spawnBoxY.transform.position.y)); 

		if (!CheckSafeToSpawn (spawnLocation)) {
			while (!CheckSafeToSpawn (spawnLocation)) {
				spawnLocation = new Vector2 (
					Random.Range (Origin.transform.position.x, spawnBoxX.transform.position.x), 
					Random.Range (Origin.transform.position.y, spawnBoxY.transform.position.y));
			}
		}

		GameObject enemy = (GameObject)Instantiate (phase2Enemy, spawnLocation, Quaternion.identity);
		
		enemy.GetComponent<Enemy> ().SetPlayer (Player.transform);
	}

	void SpawnBasicEnemy ()
	{

		Vector3 spawnLocation = new Vector3 (
			                        Random.Range (Origin.transform.position.x, spawnBoxX.transform.position.x), 
			                        Random.Range (Origin.transform.position.y, spawnBoxY.transform.position.y), 0);
		

		if (!CheckSafeToSpawn (spawnLocation)) {
			while (!CheckSafeToSpawn (spawnLocation)) {
				spawnLocation = new Vector3 (
					Random.Range (Origin.transform.position.x, spawnBoxX.transform.position.x), 
					Random.Range (Origin.transform.position.y, spawnBoxY.transform.position.y), 0);
			}
		}


		GameObject enemy = (GameObject)Instantiate (basicEnemy, spawnLocation, Quaternion.identity);
		enemy.GetComponent<Enemy> ().SetPlayer (Player.transform);

		//enemyManager.EnemyAdd (enemy);
	}

	bool CheckSafeToSpawn (Vector2 spawnLocation)
	{
		if (Vector2.Distance (spawnLocation, new Vector2 (Player.transform.position.x, Player.transform.position.y)) < safeDistance) {
			return false;
		} else {
			return true;
		}
	}

	public void ImDone ()
	{
		SpawnBasicEnemy ();
	}
}