﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Player : Pawn
{

	//Public variables
	public float speed;
	public GameObject projectile;
	public float projectileSpeed;
	public float projectileCooldown;


	//Member variables
	private bool readyToThrow;
	private bool isAttacking = false;


	//Components
	private Rigidbody2D rb2d;


	//Assets
	Animator anim;
	public AudioClip attackSound;
	public AudioClip deathSound;
	AudioSource audioSource;

	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();

		readyToThrow = true;

		anim = GetComponent<Animator>();
		//anim.SetTrigger("Attack");
		audioSource = GetComponent<AudioSource> ();
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);
        //rb2d.AddForce(movement);
        //rb2d.AddForce (movement * speed);\
        movement = movement.normalized;
		rb2d.velocity = movement * speed;

        //if (isAttacking)
        //{
        //    rb2d.velocity = movement * speed * 2;
        //}

	}

	void Update ()
	{
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		//rotate player to look at mouse
		float AngleRad = Mathf.Atan2 (mousePosition.y - rb2d.transform.position.y, mousePosition.x - rb2d.transform.position.x);
		float angle = (180 / Mathf.PI) * AngleRad;
		rb2d.rotation = angle - 90;


		Offense (mousePosition);
	}

	void Offense (Vector3 mousePosition)
	{
		//update throw cooldown
		if (!readyToThrow) {
			projectileCooldown -= Time.deltaTime;
			if (projectileCooldown <= 0) {
				readyToThrow = true;
				projectileCooldown = 5;
			}
		}


		//swing sword
		if (Input.GetButtonDown ("Fire1"))
        {
			isAttacking = true;
			audioSource.PlayOneShot (attackSound, .75f);
            Animator anim = GetComponent<Animator>();
            if (null != anim)
            {
                anim.Play("Swing");
            }
        } else {
            //if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Swing"))
            if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !anim.IsInTransition(0))
            {
                isAttacking = false;
            }
		}

		//Blink
		if (Input.GetButtonDown ("Fire2")) {
			Blink (mousePosition);
		}

		//throw 'ninja star'
		if (Input.GetButtonDown ("Jump")) {
			if (readyToThrow) {
				Throw ();
			}

		}
	}

	void Throw ()
	{
		GameObject arrow;
		arrow = (GameObject)Instantiate (projectile, transform.position, transform.rotation);
		arrow.GetComponent<Rigidbody2D> ().AddForce (gameObject.transform.up * projectileSpeed);
		readyToThrow = false;
	}

	void Blink (Vector2 destination)
	{
		this.transform.position = destination;
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		
	}

	override public void Attacked ()
    {
		audioSource.PlayOneShot (deathSound, 1f);
        SceneManager.LoadScene("Main");
    }

	public bool IsAttacking ()
	{
		return isAttacking;
	}
}