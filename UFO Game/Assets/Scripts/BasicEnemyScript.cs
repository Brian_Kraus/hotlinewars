﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Enemy))]
public class BasicEnemyScript : Pawn
{
	public float speed;

	void Start ()
	{
	}

	void FixedUpdate ()
	{
		float z = Mathf.Atan2 ((GetComponent<Enemy> ().GetPlayer ().transform.position.y - transform.position.y), 
			          (GetComponent<Enemy> ().GetPlayer ().transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;

		transform.eulerAngles = new Vector3 (0, 0, z);
		//rigidbody2D.AddForce(gameObject.transform.up * speed);
		GetComponent<Rigidbody2D> ().AddForce (gameObject.transform.up * speed);
	}


	override public void Attacked ()
	{
		Destroy (this.gameObject);
		GetComponent<Enemy> ().ImDone ();
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player")) {
			other.GetComponentInParent<Pawn> ().Attacked ();
		}
	}
}