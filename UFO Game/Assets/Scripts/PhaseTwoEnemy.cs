﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Enemy))]
public class PhaseTwoEnemy : Pawn
{

	public GameObject projectile;
	public float attackCooldown;
	public float projectileSpeed;
	public float speed;

	private bool readyToFire;

	
	// Use this for initialization
	void Start ()
	{
		readyToFire = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Offense ();
	}

	override public void Attacked ()
	{
		Destroy (this.gameObject);
		GetComponent<Enemy> ().ImDone ();
	}

	void Offense ()
	{
		if (!readyToFire) {
			attackCooldown -= Time.deltaTime;
			if (attackCooldown <= 0) {
				readyToFire = true;
				attackCooldown = 5;
			}
		}

		if (Vector2.Distance ((Vector2)GetComponent<Enemy> ().GetPlayer ().position, this.gameObject.transform.position) < 25
		    && readyToFire == true) {
			Attack ();
		}
	}


	void FixedUpdate ()
	{
		//Move toward the player
		float z = Mathf.Atan2 ((GetComponent<Enemy> ().GetPlayer ().transform.position.y - transform.position.y), 
			          (GetComponent<Enemy> ().GetPlayer ().transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90;

		transform.eulerAngles = new Vector3 (0, 0, z);
		//rigidbody2D.AddForce(gameObject.transform.up * speed);
		GetComponent<Rigidbody2D> ().AddForce (gameObject.transform.up * speed);
	}

	void Attack ()
	{
		GameObject arrow;
		arrow = (GameObject)Instantiate (projectile, transform.position, transform.rotation);
		arrow.GetComponent<Rigidbody2D> ().AddForce (gameObject.transform.up * projectileSpeed);
		readyToFire = false;
	}
}
