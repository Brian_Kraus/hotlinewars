﻿using UnityEngine;
using System.Collections;

public class Sword : MonoBehaviour
{


	public AudioClip SwordHitSound;
	AudioSource audioSource;

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void OnTriggerEnter2D (Collider2D other)
	{
		//this.transform.parent.GetComponent<YourparentScript>().yourVariable = newValue;

		if (this.GetComponentInParent<Player> ().IsAttacking () == true && other.gameObject.CompareTag ("Enemy")) {
			//TODO: tell enemy it has been hit by sword
			other.GetComponentInParent<Pawn> ().Attacked ();
			audioSource.PlayOneShot (SwordHitSound, 1f);
		}
	}
}