﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float speed;
	public bool isAttacking = false;

	private Rigidbody2D rb2d;


	Animator anim;

	void Start()
	{
		rb2d = GetComponent<Rigidbody2D> ();

		anim = GetComponent<Animator>();
		anim.SetTrigger("Attack");
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		Vector2 movement = new Vector2(moveHorizontal, moveVertical);
		//rb2d.AddForce(movement);
		//rb2d.AddForce (movement * speed);\
		rb2d.velocity = movement * speed;

	}

	void Update(){
		Vector3 mouseP = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		float AngleRad = Mathf.Atan2 (mouseP.y - rb2d.transform.position.y, mouseP.x - rb2d.transform.position.x);
		float angle = (180 / Mathf.PI) * AngleRad;
		rb2d.rotation = angle - 90;

		if (Input.GetButtonDown ("Fire1")) {
			isAttacking = true;
		} else {
			isAttacking = false;
		}
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Enemy"))
		{
			Destroy(this.gameObject);
		}
	}
}