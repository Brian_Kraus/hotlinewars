﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
	Transform player;
	SpawnScript spawnScript;

	// Use this for initialization
	public void Start ()
	{
		spawnScript = GameObject.Find ("Spawner").GetComponent<SpawnScript> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
	}

	public void Boo ()
	{
	}

	public void SetSpawnScript (SpawnScript _spawnScript)
	{	
		spawnScript = _spawnScript;
	}

	public void SetPlayer (Transform playerPosition)
	{
		player = playerPosition;
	}

	public Transform GetPlayer ()
	{
		return player;
	}

	public void ImDone ()
	{
		spawnScript.ImDone ();
	}

	//	// Update is called once per frame
	//	void Update ()
	//	{
	//
	//	}
}