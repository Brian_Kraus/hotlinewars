﻿using UnityEngine;
using System.Collections;

public abstract class Pawn : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	//public abstract void Attack ();

	public abstract void Attacked ();
}
